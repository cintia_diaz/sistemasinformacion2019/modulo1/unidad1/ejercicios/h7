﻿-- bd ejercicio 1 hoja de ejercicios 7
DROP DATABASE IF EXISTS bdh7ej1;
CREATE DATABASE bdh7ej1;
USE bdh7ej1;

CREATE OR REPLACE TABLE departamento(
  cod_dpto varchar(10),
  PRIMARY KEY (cod_dpto)
  );

CREATE OR REPLACE TABLE empleado(
  dni varchar(10),
  PRIMARY KEY (dni)
  );

CREATE OR REPLACE TABLE pertenece(
  dpto varchar(10),
  empleado varchar(10),
  PRIMARY KEY (dpto,empleado),
  UNIQUE KEY (empleado),
  CONSTRAINT fkpertenece_dpto FOREIGN KEY (dpto) REFERENCES departamento (cod_dpto),
  CONSTRAINT fkpertenece_emple FOREIGN KEY (empleado) REFERENCES empleado (dni)
  );


CREATE OR REPLACE TABLE proyecto(
  cod_proy varchar(10),
  PRIMARY KEY (cod_proy)
  );

CREATE OR REPLACE TABLE trabaja(
  empleado varchar(10),
  proyecto varchar(10),
  fecha date, 
  PRIMARY KEY(empleado, proyecto),
  CONSTRAINT fktrabaja_emple FOREIGN KEY (empleado) REFERENCES empleado (dni),
  CONSTRAINT fktrabaja_proy FOREIGN KEY (proyecto) REFERENCES proyecto (cod_proy)
  );